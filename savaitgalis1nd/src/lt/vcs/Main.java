package lt.vcs;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.Character;
/**
 *
 * @author mano
 */
public class Main {
    static Scanner sc = new Scanner(System.in);
    
    
    public static void main(String[] args) {
        out("Iveskite uzduoties numeri, kuria norite vykdyti");
        out("1 - PirmaUzduotis, 2 - Kalkuletorius, 3 - IveskZodi, 4 - SkaiciuSuma, 5 - IveskZodi2, 6 - DaugybosLentele, 7 - RaidesSakiny");
        int uzduotiesNr = sc.nextInt();
        switch (uzduotiesNr) {
            case 1:
                pirmaUzduotis();
                break;
           case 2:
                kalkuletorius();
                break;
            case 3:
                iveskZodi();
                break;
            case 4:
                skaiciuSuma();
                break;
            case 5:
                iveskZodi2();
                break;
            case 6:
                daugybosLentele();
                break;
            case 7:
                raidesSakiny();
                break;
            default:
                out("Turite pasirinkti tarp 1 ir 7 sveikuju skaiciu");
                out("Bandykite dar karta");
                main(new String[] {"a", "b", "c"});
        }
    }
    private static void out(String txt) {
        System.out.println(txt);
    }
    private static void pirmaUzduotis() {
        out("Iveskite skaiciu nuo 1 iki 10");
        int skaicius = sc.nextInt();
        
        if (skaicius < 5 && skaicius > 0) {
            out("Ivestas skaicius mazesnis uz 5");
        }
        else if (skaicius > 5 && skaicius < 11) {
            out("Ivestas skaicius didesnis uz 5");
        }
        else if (skaicius == 5) {
            out("Ivestas skaicius yra penki");
        }
        else {
            while (skaicius < 1 || skaicius > 10) {
            out("Ivestas blogas skaicius");
            PirmaUzduotis();
            break;
            }
        }
    }
    private static void kalkuletorius() {
        out("Iveskite pirma skaiciu su kuriuo norite atlikti veiksma");
        double pirmasSkaicius = sc.nextDouble();
        out("Pasirinkite skaiciu, koki veiksma norite atlikti");
        out("1 - sudetis, 2 - atimtis, 3 - daugyba, 4 - dalyba");
        int pasirinkimas = sc.nextInt();
        if (pasirinkimas < 1 || pasirinkimas > 4) {
            out("Reikia pasirinkti tarp 1 ir 4 sveikuju skaiciu veiksmui atlikti");
            out("Bandykite is naujo");
            Kalkuletorius();
        }
        out("Iveskite antra skaiciu");
        double antrasSkaicius = sc.nextDouble();
        if (pasirinkimas == 4 && antrasSkaicius == 0) {
            out("Dalyba is 0 negalima");
            System.exit(1);
        }
        switch (pasirinkimas) {
            case 1:
                System.out.println(pirmasSkaicius + " + " + antrasSkaicius + " = " + (pirmasSkaicius + antrasSkaicius));
                break;
            case 2:
                System.out.println(pirmasSkaicius + " - " + antrasSkaicius + " = " + (pirmasSkaicius - antrasSkaicius));
                break;
            case 3:
                System.out.println(pirmasSkaicius + " * " + antrasSkaicius + " = " + (pirmasSkaicius * antrasSkaicius));
                break;
            case 4:
                System.out.println(pirmasSkaicius + " / " + antrasSkaicius + " = " + (pirmasSkaicius / antrasSkaicius));
            default:
                out("Kazkas negerai");
        }
    }
    private static void iveskZodi() {
        out("Iveskite zodi");
        String zodis = sc.next();
        System.out.println("Jusu ivestame zodyje yra " + zodis.length() + " simboliai/iu");
        System.out.println(zodis.toUpperCase());
    }
    private static void skaiciuSuma() {
        out("Rasykite sveikus skaicius i ekrana, kai noresite suzinoti suma, rasykite 0");
        int suma = 0;
        int skaicius;
        List<Integer> skaiciai = new ArrayList<Integer>();
        do {
            skaicius = sc.nextInt();
            suma += skaicius;
            skaiciai.add(skaicius);
        } while (skaicius != 0);
        for (int x = 0; x < skaiciai.size(); x++) {
            if (x == skaiciai.size() - 1) {
                System.out.print(skaiciai.get(x) + " = " + suma);
                continue;
            }
            System.out.print(skaiciai.get(x) + " + ");
        }
    }
    private static void iveskZodi2() {
        String zodis;
        out("Iveskite zodi");
        do {
        zodis = sc.next();
        System.out.println(zodis);
        } while (!zodis.equalsIgnoreCase("stop"));
    }
    private static void daugybosLentele() {
        out("Iveskite skaiciu iki kokio norite matyti daugybos lentele");
        int pasirinkimas = sc.nextInt();
        for (int x = 1; x <= pasirinkimas; x++) {
            for (int i = 1; i < 11; i++) {
                System.out.println(x + " * " + i + " = " + (x * i));
            }
        }
    }
    private static void raideSakiny() {
        int raidziuSkaicius = 0;
        out("Iveskite sakini");
        String sakinys = sc.nextLine();
        sakinys = sakinys.toLowerCase();
        out("Iveskite norima raide");
        char raide = sc.next("[a-zA-Z]").charAt(0);
        raide = Character.toLowerCase(raide);
        for (int x = 0; x < sakinys.length(); x++) {
            if (sakinys.charAt(x) == raide) {
                raidziuSkaicius++;
            }
        }
        if (raidziuSkaicius == 1 || raidziuSkaicius == 21 || raidziuSkaicius == 31) {
            System.out.println("Jusu ivesta raide buvo panaudota " + raidziuSkaicius + " karta");
        }
        System.out.println("Jusu ivesta raide buvo panaudota " + raidziuSkaicius + " kartus/u");
    }
    private static void raidesSakiny() {
        int q=0; int w=0; int e=0; int r=0; int t=0; int y=0; int u=0; int i=0; int o=0; int p=0; int a=0;
        int s=0; int d=0; int f=0; int g=0; int h=0; int j=0; int k=0; int l=0; int z=0;
        int x=0; int c=0; int v=0; int b=0; int n=0; int m=0;
        out("Iveskite sakini");
        String sakinys = sc.nextLine();
        sakinys = sakinys.toLowerCase();
        for (int iter = 0; iter < sakinys.length(); iter++) {
            switch (sakinys.charAt(iter)) {
                case 'q':
                    q++;
                    break;
                case 'w':
                    w++;
                    break;
                case 'e':
                    e++;
                    break;
                case 'r':
                    r++;
                    break;
                case 't':
                    t++;
                    break;
                case 'y':
                    y++;
                    break;
                case 'u':
                    u++;
                    break;
                case 'i':
                    i++;
                    break;
                case 'o':
                    o++;
                    break;
                case 'p':
                    p++;
                    break;
                case 'a':
                    a++;
                    break;
                case 's':
                    s++;
                    break;
                case 'd':
                    d++;
                    break;
                case 'f':
                    f++;
                    break;
                case 'g':
                    g++;
                    break;
                case 'h':
                    h++;
                    break;
                case 'j':
                    j++;
                    break;
                case 'k':
                    k++;
                    break;
                case 'l':
                    l++;
                    break;
                case 'z':
                    z++;
                    break;
                case 'x':
                    x++;
                    break;
                case 'c':
                    c++;
                    break;
                case 'v':
                    v++;
                    break;
                case 'b':
                    b++;
                    break;
                case 'n':
                    n++;
                    break;
                case 'm':
                    m++;
                    break;
                default:
            }
        }
        if (q > 0) {
            System.out.println("q: " + q);
        }
        if (w > 0) {
            System.out.println("w: " + w);
        }
        if (e > 0) {
            System.out.println("e: " + e);
        }
        if (r > 0) {
            System.out.println("r: " + r);
        }
        if (t > 0) {
            System.out.println("t: " + t);
        }
        if (y > 0) {
            System.out.println("y: " + y);
        }
        if (u > 0) {
            System.out.println("u: " + u);
        }
        if (i > 0) {
            System.out.println("i: " + i);
        }
        if (o > 0) {
            System.out.println("o: " + o);
        }
        if (p > 0) {
            System.out.println("p: " + p);
        }
        if (a > 0) {
            System.out.println("a: " + a);
        }
        if (s > 0) {
            System.out.println("s: " + s);
        }
        if (d > 0) {
            System.out.println("d: " + d);
        }
        if (f > 0) {
            System.out.println("f: " + f);
        }
        if (g > 0) {
            System.out.println("g: " + g);
        }
        if (h > 0) {
            System.out.println("h: " + h);
        }
        if (j > 0) {
            System.out.println("j: " + j);
        }
        if (k > 0) {
            System.out.println("k: " + k);
        }
        if (l > 0) {
            System.out.println("l: " + l);
        }
        if (z > 0) {
            System.out.println("z: " + z);
        }
        if (x > 0) {
            System.out.println("x: " + x);
        }
        if (c > 0) {
            System.out.println("c: " + c);
        }
        if (v > 0) {
            System.out.println("v: " + v);
        }
        if (b > 0) {
            System.out.println("b: " + b);
        }
        if (n > 0) {
            System.out.println("n: " + n);
        }
        if (m > 0) {
            System.out.println("m: " + m);
        }
    }
}
