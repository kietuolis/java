package lt.vcs;

import static lt.vcs.VCSUtils.*;


public class Player extends Person {
    
    private int dice;
    private int[] diceHand;

    
    public Player(String name, String email) throws Exception {
        super(name, email);
        this.dice = random(1, 6);
    }
    public Player(String name, String email, Gender gender) throws Exception {
        this(name, email);
        this.gender = gender;
    }
    public Player(String name, String email, String surName, Gender gender, int age) throws Exception {
        this(name, email, gender);
        setSurName(surName);
        setAge(age);
    }
    @Override
    public String toString() {
        return super.toString().replaceFirst("\\)", ", dice = " + dice + ")") ;
    }

    public int getDice() {
        return dice;
    }

    public int[] getDiceHand() {
        return diceHand;
    }
    
}
