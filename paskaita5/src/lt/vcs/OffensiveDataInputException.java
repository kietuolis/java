package lt.vcs;

/**
 *
 * @author mano
 */
public class OffensiveDataInputException extends Exception {
    public OffensiveDataInputException() {
        super();
    }
    public OffensiveDataInputException(String message) {
        super(message);
    }
}
