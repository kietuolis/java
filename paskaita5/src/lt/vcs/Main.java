package lt.vcs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static lt.vcs.VCSUtils.*;
/**
 *
 * @author mano
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Player p1 = null;
        while (p1 == null) {
            try {
                p1 = new Player(inWord("ivesk varda"), inWord("ivesk email'a"));
            } catch (Exception e) {
                out(e.getMessage());
            }
        }
        try {
            Person per1 = new Person("", "");
        } catch (BadDataInputException | OffensiveDataInputException e) {
            out(e.getMessage());
        } catch (Exception e) {
            out(e.getMessage());
        }
    }
    private static Set<String> createSet(String... strings) {
        Set<String> result = new HashSet();
        if (strings != null) {
            result.addAll(Arrays.asList(strings));
        }
        return result;
    }
    private static List<String> createList(String... strings) {
        List<String> result = new ArrayList();
        if (strings != null) {
            result.addAll(Arrays.asList(strings));
        }
        return result;
    }
    private static void outCollection(Collection col) {
        if (col != null) {
            for (Object item : col) {
                out(item);
            }
        }
    }
    private static void listSet() {
        String line = inLine("iveskite betkokius zodzius atskirtus kableliu");
        line = line.replaceAll(" ", "");
        String[] lineMas = line.split(",");
        List<String> lineList = createList(lineMas);
        Set<String> lineSet = createSet(lineMas);
        Iterator<String> iter = lineList.iterator();
        while (iter.hasNext()) {
            String iterItem = iter.next();
            iter.remove();
        }
        out("--------- LineList ---------");
        outCollection(lineList);
        out("--------- LineSet ---------");
        outCollection(lineSet);
        out("--------- LineList SORTED ----------");
        Collections.sort(lineList);
        outCollection(lineList);
        out("--------- LineList SORTED REVERSE ----------");
        Collections.reverse(lineList);
        outCollection(lineList);
        Map<String, Integer> mapas = new HashMap();
        mapas.put("obuolys", 5);
        Integer val = mapas.get("obuolys");
        out(val);
        Set<String> raktai = mapas.keySet();
        Collection<Integer> reiksmes = mapas.values();
        for (Integer reiksme : reiksmes) {
            //do anything you want
        }
        for (String raktas : raktai) {
            //do anything you want
        }
    }
    
}
