package lt.vcs;

/**
 *
 * @author mano
 */
public class BadDataInputException extends Exception {
    public BadDataInputException() {
        super();
    }
    public BadDataInputException(String message) {
        super(message);
    }
}
