package lt.vcs;

/**
 *
 * @author mano
 */
public enum Gender {
    MALE("Male", "Vyras", 1),
    FEMALE("Female", "Moteris", 0),
    OTHER("Other", "Kita", 2);
    
    private String enLabel;
    private String ltLabel;
    private int id;
    
    public static Gender getById(int id) {
        for (Gender gen : Gender.values()) {
            if (id == gen.getId()) {
                return gen;
            }
        }
        return null;
    }
    
    private Gender(String enLabel, String ltLabel, int id) {
        this.enLabel = enLabel;
        this.ltLabel = ltLabel;
        this.id = id;
    }
    public String getEnLabel() {
        return enLabel;
    }
    public String getLtLabel() {
        return ltLabel;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return null;
    }
}
