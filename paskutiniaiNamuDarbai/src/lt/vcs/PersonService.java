package lt.vcs;

import static lt.vcs.VCSUtils.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author mano
 */
public class PersonService {
    public static boolean save(Person person) {
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "vcs_17_04";
        String userName = "root";
        String password = "";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            out("Valio! prisijungem prie DB, schema: " + conn.getSchema());
            Statement s = conn.createStatement();
            s.executeUpdate("insert into person values(" + (getLastId(s, "person") + 1) 
                    + ", '" + person.getName() + "', " + "'" + person.getSurName() + "', " 
                    + "'" + person.getGender().getId() + "', " + "'" + person.getAge() + "', " 
                    + "'" + person.getEmail() + "');");
            return true;
        } catch (Exception e) {
            out("Nepavyko prideti person: " + e.getMessage());
            return false;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
    }
    private static Integer getLastId(Statement s, String table) {
        Integer result = null;
        try {
            ResultSet rs = s.executeQuery("select id from  " + table
                    + " order by id desc limit 1;");
            if (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (Exception e) {
            out("Klaida traukiant paskutini id is " + table + ": " + e.getMessage());
        }
        return result;
    }
}
