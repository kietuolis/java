package lt.vcs;

import static lt.vcs.PersonService.*;

public class Main{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        Person person = new Person("Jisai", "jega@ziauriai.eu", "Jisiauskas", Gender.MALE, 15);
        save(person);
    }
    
}
