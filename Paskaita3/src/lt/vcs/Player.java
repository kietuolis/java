package lt.vcs;

import static lt.vcs.VCSUtils.*;
import static lt.vcs.Gender.*;


public class Player {
    
    private Gender gender;    
    private String name;
    private int dice;
    private int[] diceHand;
    
    public Player(String name, Gender gender) {
        this.diceHand = new int[5];
        this.gender = gender;
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        this.dice = random(1, 6);
        for (int x = 0; x < 5; x++) {
            diceHand[x] = random(1, 6);
        }
    }
    
    public String getName() {
        return name;
    }

    public int getDice() {
        return dice;
    }

    public Gender getGender() {
        return gender;
    }

    public int[] getDiceHand() {
        return diceHand;
    }
    
}
