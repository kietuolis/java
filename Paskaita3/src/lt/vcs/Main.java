package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import static lt.vcs.VCSUtils.*;
import static lt.vcs.Player.*;
import static lt.vcs.Gender.*;



public class Main {

   
    public static void main(String[] args) {
        pradziosKodes();
//        out("1 - kauliukuZaidimas, 2 - kamuoliukuZaidimas");
//        int zaidimoNumeris = inInt();
//        if (zaidimoNumeris == 1) {
//            kauliukuZaidimas();
//        } else if (zaidimoNumeris == 2) {
//            kamuoliukuZaidimas();
//        }
    }
    
    
    private static void kamuoliukuZaidimas() {
        List<Integer> pasirinktiSkaiciai = new ArrayList<>();
        List<Integer> iskriteKamuoliukai = new ArrayList<>();
        int atspetiKamuoliukai = 0;
        out("Pasirinkite statymo suma nuo 1 iki 10 euru (tik sveiki skaciai)");
        int statymoSuma = inInt();
        if (statymoSuma > 0 && statymoSuma < 11) {
            statymoSuma = statymoSuma;
        } else {
            while (statymoSuma < 1 || statymoSuma > 10) {
                out("Turite pasirinkti sveikus skaicius nuo 1 iki 10");
                statymoSuma = inInt();
            }
        }
        out("Pasirinkite savo sesis laimingus skaicius nuo 1 iki 30");
        for (int x = 0; x < 6; x++) {
            int skaicius = inInt();
            Integer iSkaicius = skaicius;
            if (skaicius > 0 && skaicius < 31 && !pasirinktiSkaiciai.contains(iSkaicius)) {
                pasirinktiSkaiciai.add(skaicius);
            } else if (pasirinktiSkaiciai.contains(iSkaicius)) {
                out("Toki skaiciu jau buvote pasirinkes, pasirinkite nauja skaiciu");
                x--;
            } else {
                out("Turite pasirinkti sveika skaiciu nuo 1 iki 30");
                x--;
            }
        }
        for (int x = 0; x < 7; x++) {
            int random = random(1, 30);
            Integer iRandom = random;
            if (!iskriteKamuoliukai.contains(random)) {
                iskriteKamuoliukai.add(random);
            } else {
                x--;
            }
        }
        for (int i = 0; i < 6; i++) {
            for (int x = 0; x < 7; x++) {
                if (pasirinktiSkaiciai.toArray()[i] == iskriteKamuoliukai.toArray()[x]) {
                    atspetiKamuoliukai++;
                }
            }
        }
        out("Pasirinkti kamuoliukai: " + pasirinktiSkaiciai);
        out("Laimingi kamuoliukai: " + iskriteKamuoliukai);
        switch (atspetiKamuoliukai) {
            case 0:
                out("Neatspejote nei vieno kamuoliuko");
                break;
            case 1:
                out("Atspejote 1 kamuoliuka ir nieko nelaimejote!");
                break;
            case 2:
                out("Atspejote 2 kamuoliukus ir nieko nelaimejote!");
                break;
            case 3:
                out("Atspejote 3 kamuoliukus ir laimejote " + 1 * statymoSuma + "€!");
                break;
            case 4:
                out("Atspejote 4 kamuoliukus ir laimejote " + 11 * statymoSuma + "€!");
                break;
            case 5:
                out("Atspejote 5 kamuoliukus ir laimejote " + 200 * statymoSuma + "€!");
                break;
            case 6:
                out("Atspejote 6 kamuoliukus ir laimejote " + 100000 * statymoSuma + "€!");
        }
    }
    
    private static void kauliukuZaidimas() {
        out("Pasirinkite ar norite zaisti su 1 kauliu ar su 5");
        System.out.print("1/5 ");
        int zaidimoTipas = inInt();
        if(zaidimoTipas == 1) {        
            out("Cia yra kauliuku zaidimas dviems zaidejams");
            out("Iveskite pirmo zaidejo varda");
            String playerName = inWord();
            out("Iveskite lyties skaiciu. 0 - moteris, 1 - vyras, 2 - kita");
            Player player = new Player(playerName, getById(inInt()));
            out("Iveskite antro zaidejo varda");
            String player1Name = inWord();
            out("Iveskite lyties skaiciu. 0 - moteris, 1 - vyras, 2 - kita");
            Player player1 = new Player(player1Name, getById(inInt()));
            out(player.getName() + " (" + player.getGender().getEnLabel() + ")" + " isrideno " + player.getDice());
            out(player1.getName() + " (" + player1.getGender().getEnLabel() + ")"  + " isrideno " + player1.getDice());
            if (player.getDice() > player1.getDice()) {
                out("laimejo: " + player.getName());
            }
            else if (player.getDice() == player1.getDice()) {
                out("pralaimejo abudu zaidejai");
            } else {
                out("laimejo: " + player1.getName());
            }
        } else if (zaidimoTipas == 5) {
            int playerSuma = 0;
            int player1Suma = 0;
            out("Cia yra kauliuku zaidimas dviems zaidejams");
            out("Iveskite pirmo zaidejo varda");
            String playerName = inWord();
            out("Iveskite lyties skaiciu. 0 - moteris, 1 - vyras, 2 - kita");
            Player player = new Player(playerName, getById(inInt()));
            out("Iveskite antro zaidejo varda");
            String player1Name = inWord();
            out("Iveskite lyties skaiciu. 0 - moteris, 1 - vyras, 2 - kita");
            Player player1 = new Player(player1Name, getById(inInt()));
            for (int x = 0; x < 5; x++) {
                playerSuma += player.getDiceHand()[x];
            }
            for (int x = 0; x < 5; x++) {
                player1Suma += player1.getDiceHand()[x];
            }
            out(player.getName() + " (" + player.getGender().getEnLabel() + ")" + " kauliuku suma: " + playerSuma + " " + Arrays.toString(player.getDiceHand()));
            out(player1.getName() + " (" + player1.getGender().getEnLabel() + ")"  + " kauliuku suma: " + player1Suma + " " + Arrays.toString(player1.getDiceHand()));
            if (playerSuma > player1Suma) {
                out("laimejo: " + player.getName());
            }
            else if (playerSuma == player1Suma) {
                out("pralaimejo abudu zaidejai");
            } else {
                out("laimejo: " + player1.getName());
            }
        } else {
            out("Turite pasirinkti 1 arba 5 kauliukus");
        }
    }
    private static void pradziosKodes() {
        out("Iveskite 3 raidziu zodi");
        String abc = null;
        String zxc = null;
        if (abc != null && abc.equals(zxc)) {
            out("tikrai abc");
        } else {
            out("ne abc");
        }
        String result = (abc != null && abc.equals(zxc)) ? "tikrai abc" : "ne abc";
        out(result);
        SimpleDateFormat sdf = new SimpleDateFormat("'{ Data:' yyyy-MM-dd 'Laikas:' HH:mm:ss:SSS }");
        out(sdf.format(new Date()));
        out(random(1, 10));
    }
}
