package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;



public class VCSUtils {

    public static void out(Object txt) {
        System.out.println(timeNow() + " " + txt);
    }
    private static Scanner newScan() {
        return new Scanner(System.in);
    }
    public static int inInt() {
        return newScan().nextInt();
    }
    public static String inLine() {
        return newScan().nextLine();
    }
    public static double inDouble() {
        return newScan().nextDouble();
    }
    public static String inWord() {
        return newScan().next();
    }
    private static String timeNow() {
        SimpleDateFormat sdf = new SimpleDateFormat("'['HH:mm:ss:SSS']'");
        return sdf.format(new Date());
    }
    public static int random(int from, int to) {
        return ThreadLocalRandom.current().nextInt(from, to + 1);
    }
    
}
