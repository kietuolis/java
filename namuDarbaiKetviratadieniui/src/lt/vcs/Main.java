package lt.vcs;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import static lt.vcs.VCSUtils.*;

/**
 *
 * @author mano
 */
public class Main {


    public static void main(String[] args) {
//        Map.Entry<String,Integer> entry =
//        new AbstractMap.SimpleEntry<String, Integer>("exmpleString", 42);
//        out(entry.getValue());
    }
    
    
    private static void sortDesc() {
        List skaiciai = new ArrayList<>(Arrays.asList(2, 5, 1, 9, -999, 9879, 55, 26));
        Collections.sort(skaiciai);
        Collections.reverse(skaiciai);
        out(skaiciai);
    }
    
    private static void sortAsc() {
        List skaiciai = new ArrayList<>(Arrays.asList(2, 5, 1, 9, -999, 9879, 55, 26));
        Collections.sort(skaiciai);
        out(skaiciai);
    }
    
    private static void palindromas() {
        List<Character> zodzioRevList = new ArrayList<>();
        List<Character> zodzioList = new ArrayList<>();
        out("Iveskite zodi ir as patikrinsiu ar jis yra palindromas");
        String zodis = inLine();
        zodis = zodis.replaceAll(" ", "");
        for (int x = 0; x < zodis.length(); x++) {
            zodzioList.add(zodis.charAt(x));
        }
        for (int x = zodis.length() - 1; x >= 0; x--) {
            zodzioRevList.add(zodis.charAt(x));
        }
        if (zodzioList.equals(zodzioRevList)) {
            out("Zodis yra palindromas");
        } else {
            out("Zodis nera palindromas");
        }
    }
}
