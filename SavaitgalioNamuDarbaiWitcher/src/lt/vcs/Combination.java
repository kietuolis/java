package lt.vcs;

/**
 * galimos kauliuku pokerio kombinacijos ir ju stiprumas, bei bonus laimejimai
 */
public enum Combination {
    NONE(0, 0),
    PAIR(1, 0),
    PAIR2(2, 5),
    KIND3(3, 10),
    STRAIGHT(4, 15),
    FULL_HOUSE(5, 20),
    KIND4(6, 25),
    KIND5(7, 30);

    private int strength;
    private int bonus;

    private Combination(int strength, int bonus) {
        this.strength = strength;
        this.bonus = bonus;
    }

    public int getStrength() {
        return strength;
    }

    public int getBonus() {
        return bonus;
    }

}
