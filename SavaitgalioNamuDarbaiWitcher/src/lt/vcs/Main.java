package lt.vcs;

import java.util.Arrays;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Main {

    static int turn;
    static int x;
    static String choice = "";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        out("Dice poker");
        String p1name = inWord("Player 1, please enter your name");
        String p2name = inWord("Player 2, please enter your name");
        p1name = p1name.substring(0, 1).toUpperCase() + p1name.substring(1).toLowerCase();
        p2name = p2name.substring(0, 1).toUpperCase() + p2name.substring(1).toLowerCase();
        Player p1;
        Player p2;
        Player nugaletojas = null;
        boolean zaisti = true;
        int zaidimoNumeris = 0;
        p1 = new Player(p1name);
        p2 = new Player(p2name);

        while (zaisti) {
            x = 0;
            p1.setBet(0);
            p2.setBet(0);
            String decision;
            Game newGame = new Game(p1, p2);
            if (zaidimoNumeris % 2 == 0) {
                newGame.setActivePlayer(p1);
            } else {
                newGame.setActivePlayer(p2);
            }
            turn = 1;
            reRaise(newGame.getActivePlayer(), getNextActivePlayer(newGame));
            getDices(newGame.getActivePlayer());
            getDices(getNextActivePlayer(newGame));
            comboToString(newGame.getActivePlayer());
            comboToString(getNextActivePlayer(newGame));
            if (x != 1) {
                out(newGame.getActivePlayer().getName() + ", do you want to bet again?");
                do {
                    decision = inWord("yes/no");
                } while (!decision.equalsIgnoreCase("yes") && !decision.equalsIgnoreCase("no"));
                if (decision.equalsIgnoreCase("yes")) {
                    turn = 3;
                    reRaise(newGame.getActivePlayer(), getNextActivePlayer(newGame));
                    diceReRoll(newGame.getActivePlayer());
                    diceReRoll(getNextActivePlayer(newGame));
                    nugaletojas = newGame.start();
                } else {
                    nugaletojas = newGame.start();
                }
            }
            if (x == 1) {
                nugaletojas = newGame.start();
            } else if (x == 2) {
                nugaletojas = newGame.getActivePlayer();
            } else if (x == 3) {
                nugaletojas = getNextActivePlayer(newGame);
            }
            setCash(newGame.getActivePlayer());
            setCash(getNextActivePlayer(newGame));
            out("Hand winner: " + nugaletojas.getName());
            nugaletojas.setCash(nugaletojas.getCash() + nugaletojas.getBet());
            if (null != nugaletojas.getHand().getCombination()
                    && nugaletojas.getHand().getCombination() != Combination.NONE
                    && nugaletojas.getHand().getCombination() != Combination.PAIR) {
                for (Combination combo : Combination.values()) {
                    if (nugaletojas.getHand().getCombination() == combo) {
                        out(nugaletojas.getName() + " received a bonus of "
                                + combo.getBonus() + " coins");
                        nugaletojas.setCash(nugaletojas.getCash() + combo.getBonus());
                    }
                }
            }
            gameWinner(newGame.getActivePlayer(), getNextActivePlayer(newGame));
            zaidimoNumeris++;
            if (p1.getCash() == 0 || p2.getCash() == 0) {
                break;
            }
            String choice = inWord("Play again? (yes/no)");
            while (!choice.equalsIgnoreCase("yes") && !choice.equalsIgnoreCase("no")) {
                choice = inWord("you have to choose between yes and no");
            }
            if (choice.equalsIgnoreCase("no")) {
                break;
            }
        }

    }

    private static void diceReRoll(Player p1) {
        String choice1;
        do {
            choice1 = inWord(p1.getName() + ", do you want to reroll your dices? (yes/no)");
        } while (!choice1.equalsIgnoreCase("yes") && !choice1.equalsIgnoreCase("no"));
        if (choice1.equalsIgnoreCase("yes")) {
            getDices(p1);
            String dicesToReRoll = inWord("enter the dice places you wish to reroll(1-5)"
                    + ", separated with a comma");
            reRollDice(p1.getHand().getHandArray(), dicesToReRoll);
            getDices(p1);
            p1.setHand(new Hand(p1.getHand().getHandArray()));
            comboToString(p1);
        }
    }

    private static void getDices(Player p1) {
        Arrays.sort(p1.getHand().getHandArray());
        out(p1.getName() + " dices: " + Arrays.toString(p1.getHand().getHandArray()));
    }

    private static void comboToString(Player player) {
        Combination combo = player.getHand().getCombination();
        switch (combo) {
            case NONE:
                out(player.getName() + " has a dice sum of " + player.getHand().getDiceSum());
                break;
            case PAIR2:
                out(player.getName() + " has two pairs");
                break;
            case KIND3:
                out(player.getName() + " has three of a kind");
                break;
            case PAIR:
                out(player.getName() + " has a pair");
                break;
            case KIND4:
                out(player.getName() + " has four of a kind");
                break;
            case FULL_HOUSE:
                out(player.getName() + " has a full house");
                break;
            case STRAIGHT:
                out(player.getName() + " has a straight");
                break;
            case KIND5:
                out(player.getName() + " has five of a kind!");
                break;
        }
    }

    private static void reRaise(Player p1, Player p2) {
        do {
            if (p1.getBet() < p2.getBet() || turn == 1 || turn == 3) {
                if (p2.getBet() > 0 && turn != 1 && turn != 3) {
                    out(p2.getName() + " total bet: " + p2.getBet());
                    do {
                        choice = inWord(p1.getName() + ", do you want to bet or quit? "
                            + "(bet/quit)");
                    } while (!choice.equalsIgnoreCase("bet") && !choice.equalsIgnoreCase("quit"));
                }
                if (choice.equalsIgnoreCase("bet") || turn == 1 || turn == 3) {
                    p1.setBet(p1.getBet() + inputCheck(p1, p2, inInt(p1.getName() + "(" + p1.getBet()
                            + "), enter your bet")));
                } else {
                    x = 3;
                }
            } else if (p1.getBet() > p2.getBet() || turn == 2) {
                if (p1.getBet() > 0) {
                    out(p1.getName() + " total bet: " + p1.getBet());
                    do {
                        choice = inWord(p2.getName() + ", do you want to bet or quit? "
                                + "(bet/quit)");
                    } while (!choice.equalsIgnoreCase("bet") && !choice.equalsIgnoreCase("quit"));
                }
                if (choice.equalsIgnoreCase("bet")) {
                    p2.setBet(p2.getBet() + inputCheck(p2, p1, inInt(p2.getName() + "(" + p2.getBet()
                            + "), enter your bet")));
                } else {
                    x = 2;
                }
            }
            turn = 0;
        } while (p1.getBet() != p2.getBet() || !choice.equalsIgnoreCase("bet"));
    }

    private static void setCash(Player p1) {
        p1.setCash(p1.getCash() - p1.getBet());
    }

    private static int inputCheck(Player p1, Player p2, int bet) {
        while (bet < 1 || bet > p1.getCash() || bet < (p2.getBet() - p1.getBet())
                || p1.getBet() + bet >= p2.getCash() && x != 1) {
            if (bet < 1) {
                bet = inInt("Bet must be higher than 0");
            } else if (bet > p1.getCash()) {
                bet = inInt("You don't have enough coins");
            } else if (bet < (p2.getBet() - p1.getBet())) {
                bet = inInt("Total bet must be atleast [" + p2.getBet() + "]."
                        + " Now your bet is: " + p1.getBet());
            } else if (p1.getBet() + bet >= p2.getCash()) {
                out(p2.getName() + " has " + p2.getCash() + " coins");
                out("both your bets are now " + p2.getCash());
                p2.setBet(p2.getCash());
                bet = p2.getCash();
                x = 1;
            }
        }
        return bet;
    }

    private static void gameWinner(Player p1, Player p2) {
        if (p1.getCash() < 1) {
            out(p1.getName() + " has no coins left");
            out("Game winner: " + p2.getName() + " with " + p2.getCash() + " coins");
        } else if (p2.getCash() < 1) {
            out(p2.getName() + " has no coins left");
            out("Game winner: " + p1.getName() + " with " + p1.getCash() + " coins");
        } else {
            out(p1.getName() + " has " + p1.getCash() + " coins");
            out(p2.getName() + " has " + p2.getCash() + " coins");
        }

    }

    public static Player getNextActivePlayer(Game game) {
        if (game.getActivePlayer().equals(game.getP1())) {
            return game.getP2();
        } else {
            return game.getP1();
        }
    }

    private static int rollDice() {
        return random(1, 6);
    }

    public static void reRollDice(int[] hand, String dices) {
        dices = dices.replaceAll(" ", "");
        for (String dice : dices.split(",")) {
            Integer nr = new Integer(dice);
            hand[nr - 1] = rollDice();
        }
    }

}
