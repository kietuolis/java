package lt.vcs;


public class Game {

    private final Player p1;
    private final Player p2;

    private Player activePlayer;

    
    public Game(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
        activePlayer = p1;
    }
    
    public Player start() {
        Player laimetojas = GameUtils.kasLaimejo(p1, p2);
        return laimetojas;
    }

    public Player getP1() {
        return p1;
    }

    public Player getP2() {
        return p2;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(Player activePlayer) {
        this.activePlayer = activePlayer;
    }
}
