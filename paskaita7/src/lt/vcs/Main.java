package lt.vcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import static lt.vcs.VCSUtils.*;

/**
 *
 * @author mano
 */
//kaip skaityti failus
public class Main {

    private static final String PVZ_FAILAS = "C:\\Temp\\pvz.txt";
    private static final String TEMP_DIR = "C:\\Temp";
    private static final String UTF_8 = "UTF-8";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File pvzFile = new File(PVZ_FAILAS);
        BufferedReader br = null;
        String failoTekstas = "";
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(pvzFile), UTF_8));
            while ((line = br.readLine()) != null) {
                failoTekstas += line + System.lineSeparator();
                out(line);
            }
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pvzFile),
                    UTF_8));
            bw.append(failoTekstas);
            bw.append("ketvirta eilute");
            bw.flush();
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
    }

    private static File newFile(String dir, String fileName) {
        File result = null;
        if (dir != null && fileName != null) {
            File direct = new File(dir);
            if (direct.isDirectory()) {
                if (!direct.exists()) {
                    try {
                        direct.mkdirs();
                    } catch (Exception e) {
                        out(e.getMessage());
                    }
                }
                result = new File(direct, fileName);
                try {
                    result.createNewFile();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        } else {
            out("Blogi parametrai");
        }
        return result;
    }

}
