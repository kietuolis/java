package lt.vcs;

import java.util.Arrays;
import static lt.vcs.KortuUtils.*;
import static lt.vcs.VCSUtils.*;
import static lt.vcs.Karas.*;
import static lt.vcs.Player.*;

/**
 *
 * @author mano
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        Ragana.start();
        
    }
}
