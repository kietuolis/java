package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static lt.vcs.VCSUtils.*;
import static lt.vcs.KortuUtils.*;

public class Ragana {

    static Player p1;
    static Player p2;
    static Player p3;
    static Player p4;
    static Player p5;
    static Player p6;
    static Player pralaimetojas;
    static List<Korta> p1Kortos;
    static List<Korta> p2Kortos;
    static List<Korta> p3Kortos;
    static List<Korta> p4Kortos;
    static List<Korta> p5Kortos;
    static List<Korta> p6Kortos;
    static Player[] players = {p1, p2, p2, p4, p5, p6};
    static List<List> playerKortos = Arrays.asList(p1Kortos, p2Kortos, p3Kortos, p4Kortos, p5Kortos, p6Kortos);
    static String pick;
    static Korta pickCard;
    static int playerNumber;

    public static void start() throws Exception {
        do {
            playerNumber = inInt("How many players will play this game?");
        } while (playerNumber < 2 || playerNumber > 6);
        for (int x = 0; x < playerNumber; x++) {
            players[x] = new Player(inWord("Player " + (x + 1) + ", enter your name"));
            playerKortos.set(x, new ArrayList());
        }
        kortos.clear();
        sukurtiKalade();
        kortos.remove(49);
        Collections.shuffle(kortos);
        padalinti();
        for (int x = 0; x < playerNumber; x++) {
            removeDouble(players[x]);
            out(players[x].getName() + " has " + players[x].getCards().size() + " cards left");
        }
        while (playerNumber > 1) {
            for (int x = 0; x < playerNumber; x++) {
                if (x == playerNumber - 1) {
                    pickCard(players[x], players[0]);
                    removeDouble(players[x]);
                    if (players[0].getCards().isEmpty()) {
                        out(players[0].getName() + " has no cards left and is out of the game");
                        players = removePlayer(players, players[0]);
                        playerNumber = playerNumber - 1;
                        x -=1;
                    }
                } else {
                    pickCard(players[x], players[x + 1]);
                    removeDouble(players[x]);
                    if (players[x + 1].getCards().isEmpty()) {
                        out(players[x + 1].getName() + " has no cards left and is out of the game");
                        players = removePlayer(players, players[x + 1]);
                        playerNumber = playerNumber - 1;
                    }
                }
                if (players[x].getCards().isEmpty()) {
                    out(players[x].getName() + " has no cards left and is out of the game");
                    players = removePlayer(players, players[x]);
                    playerNumber = playerNumber - 1;
                }
            }
        }
        out("Zaidima pralaimejo: " + players[0].getName());
    }

    private static void padalinti() {
        for (int i = 0; i < playerNumber; i++) {
            for (int x = 0; x < (51 / playerNumber); x++) {
                playerKortos.get(i).add(kortos.get(x + (i * (51 / playerNumber))));
            }
            players[i].setCards(playerKortos.get(i));
        }
        if (playerNumber == 2 || playerNumber == 5) {
            players[random(0, playerNumber - 1)].addCard(kortos.get(50));
        } else if (playerNumber == 4) {
            players[random(0, 1)].addCard(kortos.get(48));
            players[2].addCard(kortos.get(49));
            players[3].addCard(kortos.get(50));
        } else if (playerNumber == 6) {
            players[random(0, 1)].addCard(kortos.get(48));
            players[random(2, 3)].addCard(kortos.get(49));
            players[random(4, 5)].addCard(kortos.get(50));
        }
    }

    private static void removeDouble(Player player) {
        List<Korta> tempList = new ArrayList();
        for (int x = 0; x < player.getCards().size(); x++) {
            for (int i = x + 1; i < player.getCards().size(); i++) {
                if (player.getCards().get(x).getSimbolis().equalsIgnoreCase(player.getCards().get(i).getSimbolis())) {
                    if (!tempList.contains(player.getCards().get(x))
                            && !tempList.contains(player.getCards().get(i))) {
                        out(player.getName() + " match: " + player.getCards().get(x).getZenklai()
                                + " + " + player.getCards().get(i).getZenklai());
                        tempList.add(player.getCards().get(x));
                        tempList.add(player.getCards().get(i));
                    }
                }
            }
        }
        player.getCards().removeAll(tempList);
    }

    private static void pickCard(Player p1, Player p2) {
        do {
            pick = inWord(p1.getName() + ", enter <P> - pick a random card from " + p2.getName()
                    + " <L> - cards left");
        } while (!pick.equalsIgnoreCase("p") && !pick.equalsIgnoreCase("l"));
        if (pick.equalsIgnoreCase("l")) {
            out(p1.getName() + " cards:");
            for (int x = 0; x < p1.getCards().size(); x++) {
                System.out.print(p1.getCards().get(x).getZenklai() + " ");
            }
        }
        pickCard = p2.getCards().get(random(0, p2.getCards().size() - 1));
        out("\n" + p1.getName() + ", you picked: " + pickCard.getZenklai());
        p1.addCard(pickCard);
        p2.removeCard(pickCard);
        pick = "";
    }

    private static Player[] removePlayer(Player[] players, Player p) {
        for (int i = 0; i < players.length; i++) {
            if (players[i] == p) {
                Player[] copy = new Player[players.length - 1];
                System.arraycopy(players, 0, copy, 0, i);
                System.arraycopy(players, i + 1, copy, i, players.length - i - 1);
                return copy;
            }
        }
        return players;
    }

}
