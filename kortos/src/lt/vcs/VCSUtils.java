package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;



public class VCSUtils {
    //timeNow() + " " +   out() time now

    public static void out(Object txt) {
        System.out.println(txt);
    }
    private static Scanner newScan() {
        return new Scanner(System.in);
    }
    public static int inInt() {
        return newScan().nextInt();
    }
    public static int inInt(String txt) {
        out(txt);
        return inInt();
    }
    public static String inLine() {
        return newScan().nextLine();
    }
    public static String inLine(String txt) {
        out(txt);
        return inLine();
    }
    public static double inDouble() {
        return newScan().nextDouble();
    }
    public static String inWord() {
        return newScan().next();
    }
    public static String inWord(String txt) {
        out(txt);
        return inWord();
    }
    private static String timeNow() {
        SimpleDateFormat sdf = new SimpleDateFormat("'['HH:mm:ss:SSS']'");
        return sdf.format(new Date());
    }
    public static int random(int from, int to) {
        return ThreadLocalRandom.current().nextInt(from, to + 1);
    }
}
