package lt.vcs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static lt.vcs.Zenklas.*;

/**
 *
 * @author mano
 */
public class KortuUtils {
    public static List<Korta> kortos = new ArrayList();
    
    public static void maisyti(List<Korta> kortos) {
        if (kortos != null) {
            Collections.shuffle(kortos);
        }
    }
    
    public static List<Korta> sukurtiKalade() {
        String[] simboliai = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        for (Zenklas zenklas : Zenklas.values()) {
            for (String simbolis : simboliai) {
                Korta korta = new Korta(simbolis, zenklas);
                kortos.add(korta);
            }
        }
        return kortos;
    }
    public static Player winner(Player player, Player computer, int cardNumber) {
        Player winner;
        int playerScore = 0;
        int computerScore = 0;
        switch (player.getCards().get(cardNumber).getSimbolis()) {
            case "A":
                playerScore = 14;
                break;
            case "K":
                playerScore = 13;
                break;
            case "Q":
                playerScore = 12;
                break;
            case "J":
                playerScore = 11;
                break;
            case "10":
                playerScore = 10;
                break;
            case "9":
                playerScore = 9;
                break;
            case "8":
                playerScore = 8;
                break;
            case "7":
                playerScore = 7;
                break;
            case "6":
                playerScore = 6;
                break;
            case "5":
                playerScore = 5;
                break;
            case "4":
                playerScore = 4;
                break;
            case "3":
                playerScore = 3;
                break;
            case "2":
                playerScore = 2;
                break;
            
        }
        switch (computer.getCards().get(cardNumber).getSimbolis()) {
            case "A":
                computerScore = 14;
                break;
            case "K":
                computerScore = 13;
                break;
            case "Q":
                computerScore = 12;
                break;
            case "J":
                computerScore = 11;
                break;
            case "10":
                computerScore = 10;
                break;
            case "9":
                computerScore = 9;
                break;
            case "8":
                computerScore = 8;
                break;
            case "7":
                computerScore = 7;
                break;
            case "6":
                computerScore = 6;
                break;
            case "5":
                computerScore = 5;
                break;
            case "4":
                computerScore = 4;
                break;
            case "3":
                computerScore = 3;
                break;
            case "2":
                computerScore = 2;
                break;
        }
        if (playerScore > computerScore) {
            winner = player;
        } else if (computerScore > playerScore) {
            winner = computer;
        } else {
            winner = null;
        }
        return winner;
    }
}
