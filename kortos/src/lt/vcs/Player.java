package lt.vcs;

import java.util.List;



public class Player extends Person {
    private List <Korta> cards;
    
    
    public Player(String name) throws Exception {
        super(name);
    }

    public List <Korta> getCards() {
        return cards;
    }

    public void setCards(List <Korta> cards) {
        this.cards = cards;
    }
    public void addCard(Korta card) {
        this.cards.add(card);
    }
    public void removeCard(Korta card) {
        this.cards.remove(card);
    }
}
