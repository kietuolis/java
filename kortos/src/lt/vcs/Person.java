package lt.vcs;

public class Person {

    private String name;

    public Person(String name) throws Exception {
        if (name == null || name.trim().length() == 0) {
            throw new Exception("vardas yra privalomas");
        } else {
            this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        }
}

    public String getName() {
        return name;
    }
}
