package lt.vcs;

/**
 *
 * @author mano
 */
public class Korta {
    private String simbolis;
    private Zenklas zenklas;
    
    public Korta(String simbolis, Zenklas zenklas) {
        this.simbolis = simbolis;
        this.zenklas = zenklas;
    }

    public String getSimbolis() {
        return simbolis;
    }

    public Zenklas getZenklas() {
        return zenklas;
    }
    public String getZenklai() {
        return zenklas + simbolis;
    }
}
