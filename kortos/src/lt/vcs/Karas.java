package lt.vcs;

import java.awt.Choice;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static lt.vcs.VCSUtils.*;
import static lt.vcs.KortuUtils.*;

public class Karas {

    static Player player;
    static Player computer;
    static List<Korta> playerKortos;
    static List<Korta> playerKitosKortos;
    static List<Korta> computerKortos;
    static List<Korta> computerKitosKortos;
    static List<Korta> karoKruva;
    static Player nugaletojas;

    public static void start() throws Exception {
        kortos.clear();
        out("Welcome to war versus computer, letter c - play a card, s - check stats");
        String choice;
        karoKruva = new ArrayList();
        playerKortos = new ArrayList();
        playerKitosKortos = new ArrayList();
        computerKortos = new ArrayList();
        computerKitosKortos = new ArrayList();
        player = new Player(inWord("Enter your name"));
        computer = new Player("computer");
        sukurtiKalade();
        maisyti(kortos);
        padalinti();
        nugaletojas = null;
        while (nugaletojas == null) {
            System.out.print("computer: " + computerKortos.get(0).getZenklai());
            do {
                choice = inWord("       enter <C> or <S>");
            } while (!choice.equalsIgnoreCase("c") && !choice.equalsIgnoreCase("s"));
            if (choice.equalsIgnoreCase("s")) {
                out(player.getName() + " hand size: " + playerKortos.size() + 
                        " kitas pile size: " + playerKitosKortos.size());
                out("computer hand size: " + computerKortos.size() + 
                        " kitas pile size: " + computerKitosKortos.size());
            }
            out(player.getName() + ": " + playerKortos.get(0).getZenklai());
            Player winner = winner(player, computer, 0);
            if (winner == player) {
                playerKitosKortos.add(computerKortos.get(0));
                playerKitosKortos.add(playerKortos.get(0));
                computerKortos.remove(0);
                playerKortos.remove(0);
                out("winner: " + player.getName());
                if (!karoKruva.isEmpty()) {
                    playerKitosKortos.addAll(karoKruva);
                    karoKruva.clear();
                }
            } else if (winner == computer) {
                computerKitosKortos.add(computerKortos.get(0));
                computerKitosKortos.add(playerKortos.get(0));
                computerKortos.remove(0);
                playerKortos.remove(0);
                out("winner: computer");
                if (!karoKruva.isEmpty()) {
                    computerKitosKortos.addAll(karoKruva);
                    karoKruva.clear();
                }
            } else {
                out("Waaaaaaar!");
                karoKruva.add(computerKortos.get(0));
                karoKruva.add(playerKortos.get(0));
                computerKortos.remove(0);
                playerKortos.remove(0);
            }
            if (playerKortos.isEmpty() && !playerKitosKortos.isEmpty()) {
                Collections.shuffle(playerKitosKortos);
                playerKortos.addAll(playerKitosKortos);
                playerKitosKortos.clear();
                out("Baigesi " + player.getName() + " kortos, mix & take from war pile");
            }
            if (playerKortos.isEmpty() && playerKitosKortos.isEmpty() && karoKruva.isEmpty()) {
                nugaletojas = computer;
                break;
            }
            if (computerKortos.isEmpty() && !computerKitosKortos.isEmpty()) {
                Collections.shuffle(computerKitosKortos);
                computerKortos.addAll(computerKitosKortos);
                computerKitosKortos.clear();
                out("Baigesi computer kortos, mix & take from war pile");
            }
            if (computerKortos.isEmpty() && computerKitosKortos.isEmpty() && karoKruva.isEmpty()) {
                nugaletojas = player;
                break;
            }
        }
        out("Nugaletojas: " + nugaletojas.getName());
    }

    private static void padalinti() {

        for (int x = 0; x < 26; x++) {
            playerKortos.add(kortos.get(x));
            player.setCards(playerKortos);
        }
        for (int x = 0; x < 26; x++) {
            computerKortos.add(kortos.get(x + 26));
            computer.setCards(computerKortos);
        }
    }
}
