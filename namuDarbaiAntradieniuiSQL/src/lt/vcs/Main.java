package lt.vcs;

import static lt.vcs.VCSUtils.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mano
 */
public class Main {

    private static final String PVZ_FAILAS = "C:\\temp\\pvz.txt";
    private static final String TEMP_DIR = "C:\\temp";
    private static final String UTF_8 = "UTF-8";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File pvzFile = new File(PVZ_FAILAS);
        List<Integer> list = new ArrayList();
        BufferedReader br = null;
        String text;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(pvzFile),
                    UTF_8));
            while ((text = br.readLine()) != null) {
                list.add(Integer.parseInt(text));
            }
            out(list);
            
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
    }
    private static void inputLetter() {
        File pvzFile = new File(PVZ_FAILAS);
        List<String> list = new ArrayList();
        BufferedReader br = null;
        String failoTekstas = "";
        String line;
        String chr = inWord("enter the first letter of the names you wish to see");
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(pvzFile),
                    UTF_8));
            while ((line = br.readLine()) != null) {
                failoTekstas += line + System.lineSeparator();
                if (line.startsWith(chr.toLowerCase()) || line.startsWith(chr.toUpperCase())) {
                    list.add(line);
                }
            }
            Collections.sort(list);
            out(list);
            
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
    }
    private static void readAsc() {
        File pvzFile = new File(PVZ_FAILAS);
        List<String> list = new ArrayList();
        BufferedReader br = null;
        String failoTekstas = "";
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(pvzFile),
                    UTF_8));
            while ((line = br.readLine()) != null) {
                failoTekstas += line + System.lineSeparator();
                list.add(line);
            }
            Collections.sort(list);
            out(list);
            
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
    }
}
