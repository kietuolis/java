package lt.vcs;

/**
 *
 * @author mano
 */
interface IdAble {
    int getId();
}
