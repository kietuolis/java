package lt.vcs;

/**
 *
 * @author mano
 */
interface Named extends IdAble {
    
    public static Object newObj() {
        return new Object();
    }
    String getName() throws BadDataInputException;
}
