package lt.vcs;
    //<T> Person tipo parametras
public class User<T extends Person> {
    private T person;
    
    public User(T person) {
        this.person = person;
    }

    public T getPerson() {
        return person;
    }
}
