package lt.vcs;

import java.io.IOException;

public class Person implements Named {
    private String name;
    private String surName;
    protected Gender gender;
    private int age;
    private String email;
    
    public Person(String name, String email) throws BadDataInputException, OffensiveDataInputException {
        if (name == null || name.trim().length() == 0) {
            throw new BadDataInputException("vardas yra privalomas");
        } else {
            this.name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        }
        if (email == null || email.trim().length() == 0 || "nop".equals(email)) {
            throw new OffensiveDataInputException("email yra privalomas");
        } else {
            this.email = email;
        }
    }
    public Person(String name, String email, Gender gender) throws Exception {
        this(name, email);
        this.gender = gender;
    }
    public Person(String name, String email, String surName, Gender gender, int age) throws Exception {
        this(name, email, gender);
        this.surName = surName;
        this.age = age;
    }
    
    @Override
    public String toString() {
        return getClass().getSimpleName() + "(name = " + getName() + ", gender = " + gender.getEnLabel() + ")";
    }

    @Override
    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int getId() {
        return age;
    }
}
