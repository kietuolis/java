package lt.vcs;

import static lt.vcs.VCSUtils.*;

/**
 *
 * @author mano
 */
public abstract class AbstractDaiktas implements IdAble{
    
    public abstract Object naujasObjektas();
    
    public void bendrasFunkcionalumas() {
        out("" + naujasObjektas() + getId());
    }
}
