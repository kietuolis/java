package lt.vcs;

import java.util.ArrayList;
import java.util.List;
import static lt.vcs.VCSUtils.*;

/**
 *
 * @author mano
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Named g1 = Gender.FEMALE;
        Named g2 = null;
        try {
            new Person("Paul", "youl");
        } catch (Exception e) {
        }
        List<Named> namedList = new ArrayList();
        namedList.add(g1);
        namedList.add(g2);
        Named.newObj();
        for (Named named : namedList) {
            try {
                out(named.getName());
                if (named instanceof IdAble) {
                    IdAble idable = (IdAble) named;
                    out(idable.getId());
                }
            } catch (Exception e) {

            }
        }
        out("belenkas:");
        AbstractDaiktas ad = new Belenkas();
        ad.bendrasFunkcionalumas();
        out("belenkas2:");
        AbstractDaiktas ad2 = new Belenkas2();
        ad2.bendrasFunkcionalumas();
    }

}
