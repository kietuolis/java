package lt.vcs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import static lt.vcs.VCSUtils.*;

/**
 *
 * @author mano
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        
        String zodis = inLine("iveskite norima zodi ar sakiny ir suzinosite kiek jame kokiu raidziu");
        zodis = zodis.replaceAll(" ", "");
        zodis = zodis.toLowerCase();
        List<String> raides = new ArrayList();
        for (int x = 0; x < zodis.length(); x++) {
            raides.add(zodis.substring(x, x + 1));
        }
        Map<String, Integer> raidziuMap = new HashMap();
        for (String raide : raides) {
            if (!raidziuMap.containsKey(raide)) {
                raidziuMap.put(raide, 1);
            } else if (raidziuMap.containsKey(raide)) {
                raidziuMap.replace(raide, raidziuMap.get(raide), raidziuMap.get(raide) + 1);
            } else {
                out("Something went wrong");
            }
        }
        List<Integer> values = new ArrayList();
        List<String> keys = new ArrayList();
        for (Integer value : raidziuMap.values()) {
            values.add(value);
        }
        for (String key : raidziuMap.keySet()) {
            keys.add(key);
        }
        Integer max = Collections.max(values);
        
        
        out(raidziuMap);
    }
    
}
