package lt.vcs;

public class Person {
    private String name;
    private String surName;
    protected Gender gender;
    private int age;
    
    public Person(String name) {
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
    }
    public Person(String name, Gender gender) {
        this(name);
        this.gender = gender;
    }
    public Person(String name, String surName, Gender gender, int age) {
        this(name, gender);
        this.surName = surName;
        this.age = age;
    }
    
    @Override
    public String toString() {
        return getClass().getSimpleName() + "(name = " + getName() + ", gender = " + gender.getEnLabel() + ")";
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
