package lt.vcs;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static lt.vcs.VCSUtils.*;


public class Main {


    public static void main(String[] args) {
        Player p1 = new Player(inWord("Iveskite varda"), Gender.getById(inInt("Iveskite lyti")));
        Person per1 = p1;
        Object obj = p1;
        Object[] mas = {p1, per1, obj, "stringas"};
        out(p1);
        out(per1);
        out(obj);
        
        User<Person> u1 = new User(per1);
        User<Player> u2 = new User(p1);
        out(u1.getPerson());
        out(u2.getPerson());
        
        List<String> strList = new ArrayList();
        strList.add("bla");
        strList.add("bla");
        for (String bla : strList) {
            out(bla);
        }
        
        out ("----------------set'as----------");
        Set<String> strSet = new HashSet();
        strSet.add("bla");
        strSet.add("bla");
        for (String bla : strSet) {
            out(bla);
        }
    }
    
}
