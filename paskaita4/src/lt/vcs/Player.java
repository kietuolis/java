package lt.vcs;

import static lt.vcs.VCSUtils.*;


public class Player extends Person {
    
    private int dice;
    private int[] diceHand;

    
    public Player(String name) {
        super(name);
        this.dice = random(1, 6);
    }
    public Player(String name, Gender gender) {
        this(name);
        this.gender = gender;
    }
    public Player(String name, String surName, Gender gender, int age) {
        this(name, gender);
        setSurName(surName);
        setAge(age);
    }
    @Override
    public String toString() {
        return super.toString().replaceFirst("\\)", ", dice = " + dice + ")") ;
    }

    public int getDice() {
        return dice;
    }

    public int[] getDiceHand() {
        return diceHand;
    }
    
}
