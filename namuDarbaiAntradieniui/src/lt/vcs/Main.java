package lt.vcs;
import static java.lang.Math.round;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
/**
 *
 * @author mano
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        kMI();
    }
    private static void pasirinkSkaicius() {
        List<Integer> skaiciai = new ArrayList<>();
        out("Kiek skaiciu norite ivesti?");
        int skaicius = inInt();
        out("Rasykite " + skaicius + " skaicius/iu:");
        for (int x = 0; x < skaicius; x++) {
            int ivestasSkaicius = inInt();
            if (ivestasSkaicius > 100) {
                skaiciai.add(ivestasSkaicius);
            }
        }
        out("Ivesti skaiciai, didesni uz 100: " + skaiciai);
    }
    private static void kMI() {
        out("Iveskite savo svori");
        double svoris = inDouble();
        out("Iveskite savo ugi centimetrais");
        double ugis = inDouble() / 100;
        out("Jusu KMI yra: " + round(svoris / (ugis * ugis)));
    }
    private static void penkiZodziai() {
        String[] zodziai = new String[5];
        out("Iveskite penkis zodzius");
        for (int x = 0; x < 5; x++) {
            zodziai[x] = inStr();
        }
        out(Arrays.toString(zodziai));
    }
    private static void skaiciuSuma() {
        int suma = 0;
        int[] skaiciai = new int[5];
        out("Iveskite penkis skaicius");
        for (int x = 0; x < 5; x++) {
            skaiciai[x] = inInt();
            suma += skaiciai[x];
        }
        out("Jusu ivestu skaiciu suma: " + suma);
        System.out.println("Jusu ivesti skaiciai: " + Arrays.toString(skaiciai));
    }
    private static void out(String txt) {
        System.out.println(txt);
    }
    private static Scanner newScan() {
        return new Scanner(System.in);
    }
    private static int inInt() {
        return newScan().nextInt();
    }
    private static String inStr() {
        return newScan().next();
    }
    private static double inDouble() {
        return newScan().nextDouble();
    }
}
